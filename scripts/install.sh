#!/bin/bash
set -u
set -e

ln -f -s ~/dotfiles/tmux/.tmux.conf ~/.tmux.conf
ln -f -s ~/dotfiles/vim ~/.vim
ln -f -s ~/dotfiles/vim/vimrc ~/.vimrc
ln -f -s ~/dotfiles/git/gitconfig ~/.gitconfig
